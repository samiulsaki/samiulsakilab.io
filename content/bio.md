+++ 
title = "Bio" 
slug = "bio"
+++

_Here are additional information about me_

## Achievements

###### Sept 2022 : [Certified SAFe® 5 DevOps Practitioner](https://scaledagile.com/training/safe-devops/)
> **Scaled Agile Inc.**
>
> [![sdp](/certificates/certified-safe-5-devops-practitioner.png)](https://www.credly.com/badges/1754e3c7-c72a-4925-9bf7-ae18bc5c4ed2 "Click to verify my SAFe® 5 DevOps Practitioner certificate")
---

###### Feb 2022 : [HashiCorp Certified: Terraform Associate](https://www.hashicorp.com/certification/terraform-associate)
> **HashiCorp**
>
> [![terraform](/certificates/hashicorp-certified-terraform-associate.png)](https://www.credly.com/badges/be3a447b-921a-45bc-938f-d0cafa6af3b8 "Click to verify my Terraform associate certificate")
---

###### Jul 2021 : [AWS Certified Developer – Associate](https://aws.amazon.com/certification/certified-developer-associate)
> **Amazon Web Services Training and Certification**
>
> [![aws-associate](/certificates/aws-certified-developer-associate.png)](https://www.credly.com/badges/7ca788f3-ef59-43e9-bc81-34ef7034a382 "Click to verify my AWS associate certificate")
---

##### Sep 2020 : [Certified Kubernetes Administrator](https://www.cncf.io/certification/cka/) 
> **The Linux Foundation**
>
> [![cka](/certificates/cka-certified-kubernetes-administrator.png)](https://www.youracclaim.com/badges/4990e0ae-eea3-4543-8493-96d5be93416c "Click to verify my CKA certificate")
---

##### Aug 2017 : Continuous Delivery Training
> **Code Academy, Praqma**
>
> - Agile task grooming
> - Advanced version control with Git
> - Build and Test, automation with Jenkins
> - Build, Ship, Run with Docker    
---

##### Aug 2016 – May 2018 : Masters in Network and System Administration
> **University of Oslo (UiO), Oslo, Norway**
>
> Thesis: 
> - "Autonomous Virtual Machines for Ensuring QoS of Distributed Web Services using Evolutionary Game Theory"
---

##### Aug 2014 – May 2016 : Masters in Information and Communication Technology
> **University of Agder (UiA), Grimstad, Norway**
>
> Thesis: 
> - “Long Distance, Low Data Rate Packet Transmission at 868 MHz: Protocol Design and Implementation”
> - "Energy Harvesting-aware Backoff Algorithms for Distributed Device-to-Device Communication"
---

##### Aug 2001 – May 2006 : Bachelor in Computer Science & Engineering
> **University of Science and Technology (USTC), Chittagong, Bangladesh**

***
***

## Career

###### Aug 2021 - Present : Senior Engineer (Open Source Cloud), Sopra Steria, Oslo, Norway
- Mar 2022 - Present : **DevOps Engineer - Platform (Hired)**, Telia Company AB, Stockholm, Sweden
    - **DevOps** : Ansible (IAC), CI/CD, Github Actions, Docker
    - **Monitoring** : Prometheus, Grafana, Splunk
    - **Security** : TSPAM, Fireproof, One Workflow, IDM
    - **Network** : CMDB
    - **Developer** : Bash, Python
    - **SAFe** : ServiceNow, Jira, Confluence

- Sept 2021 - Feb 2022 : **DevSecOps Consultant - Cloud-Operation (Hired)**, Municipality of Oslo (Bymiljøetaten), Oslo, Norway
    - **DevOps** : Terraform (IAC), CI/CD, Github Actions, AWS, CloudFormation, IAM, Lambda, ECS, ECR, CodePipeline, EKS, Fargate, Api gateway
    - **Security** : SSO, AWS Shield, Amazon Cognito
    - **Developer** : Python, Bash, Ardoq
    - **Tester** : OKCTL (Bymiljøetaten)
    - **Monitoring** : Cloudwatch, Grafana, Prometheus
---

###### May 2019 - Aug 2021 : Senior DevOps Consultant, IT Consult AS, Oslo, Norway
- Oct 2019 - Aug 2021 : **Operation Consultant (Hired)**, Ruter AS, Oslo, Norway
    -  **SLA/ITIL - Single Point of Contact (SPOC)**
    -  **Monitoring of services** : AppDynamics (Big data, infrastructure, business transactions, analytics metrics, backend applications, IAM, HTTP method collectors), Application Manager (Machine Engine), Datadog, Splunk, StatusPage
    -  **Infrastructure** : AWS (Cloud-formation, Cloud-watch, S3 bucket, EC2 instances, EKS, ECS), ACL, Helm
    -  **Atlassian Products** : Atlassian (Jira, Bitbucket, Confluence), IAM
    -  **Database** : MySQL, PosgreSQL
    -  **DevOps** :  Gitlab, Teamcity, Terraform, Octopus, CI/CD
    -  **Programming** : Python, JavaScript, PowerShell, C++
    -  **Kubernetes/Docker** : EKS, Micro-services
    -  **Diverse** : Microsoft Intune, MobileIron, MDM, Postman
---

###### May 2018 - Apr 2019 : IT Consultant/System Administrator, Hovedkvarteret IT, Oslo, Norway
- **IAM** : IAM (Identity and Access Management), SQL, Jlro, PHP
- **Automation** : Infrastructure as Code (IaC), Ansible, Munki, Apple Configurator, AutoPkg, Python, Bash, OSAScript, ProfileCreator, PkgUtil, Pacifist, PHP, MySQL, Apache2
- **DevOps** : MDM (Mobile Device Management) Configuration
- **Monitoring/Analysing** : Zabbix, Check_MK, StatusCake, Observium, PushOver
- **Developer** : Git, MySQL, Jlro, PHP, Python, Bash, Zimbra CLI, Bitbucket, Jira, Confluence, PostrgreSQL
- **Cloud Platform** : Azure, Google, AWS
---

###### Jan 2017 - May 2018 : Teaching Assistant, Oslo Metropolitan University (OsloMet), Oslo, Norway
- Jan 2018 - May 2018 : **Systems Automation and Scripting**
    - Python, OpenStack, Linux, Django
- Jan 2017 - May 2017 : **Network and Cloud Services**
    - Wireshark, TCP, UDP, OpenStack, JavaScript, Socket Programming
---

##### Nov 2008 - Jan 2010 : Supervisor, Yates Leicester Square, London, UK
- Responsible for checkout systems, opening and closing of the nightclub
---

##### Oct 2007 - Nov 2008 : Supervisor, Subway, Leicester Square, UK
- Responsible for checkout systems, opening and closing of the workplace
---

##### Sep 2006 - Aug 2007 : Assistant Network Analyser, Warid Telecom, Bangladesh
- Maintenance and troubleshooting of infrastructure between mainframe networks.
- Simplification and explanation of technical information to others in the team.
- Planning a business strategy with the team.
- Monitoring, analysis and reporting of incoming/outgoing data.
- Network and switch configuration.
- SQL, Visual Basic, C, Java, Perl, Ruby, Cisco Switcher, IPTables rules.

***
***