+++
date = "2016-05-18"
title = "Energy Harvesting-aware Backoff Algorithms for Distributed Device-to-Device Communication"
slug = "post-9"
authors = ["Samiul Saki"]
categories = ["Publication"]
tags = ["D2D Communication", "CSMA/CA MAC Protocol"]
+++

### Publication
> Due to limited energy capacity in mobile devices, energy harvesting is regarded as a promising technology for lifetime extension in device-to-device (D2D) communication. However, medium access control (MAC) protocols designed for D2D communication intrinsically do not consider the existence of harvested energy. 
>
> In this paper we study four backoff (BO) algorithms, among which one algorithm is proposed to enhance the BO selection based on energy harvesting rate and residual energy of devices. 
>
> Simulations are performed in order to assess how the proposed algorithm can extend the lifetime of a D2D communication network as well as to provide fair channel access opportunities among devices.
>
> DOI: [10.1109/VTCSpring.2016.7504226](http://ieeexplore.ieee.org/document/7504226/)
>
> IEEE VTC 2016-Spring, 186-49514, 15-18 May 2016