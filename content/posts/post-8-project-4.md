+++
date = "2016-05-15"
title = "Data Rate Packet Transmission at 868 MHz: Protocol Design and Implementation"
slug = "post-8"
authors = ["Samiul Saki"]
categories = ["Thesis"]
tags = ["Network Protocol"]
+++

### Masters Thesis
> Data transmission over wireless networks can be achieved in various ways. Some of these technologies are feasible in many real life scenarios, but there are many consequences we have to consider in this field, such as packet loss, quality degradation of radio signals, ambient noise, transmission delay and etc. For the low data rate packet transmission, to adapt and to address IoT is one of the most discussed topics nowadays, especially in terms of transmission protocol design and the implementation of that design in real life scenarios. Transmitting packets in low data rate over long distance efficiently is becoming an important issue in this fast-paced modern world. Our objective is to achieve long distance with low data rate packet transmission by combining them in the same protocol design with diverse form of services.
>
> In this project, we design an embedded solution and implement our own transmission schemes based on Raspberry Pi micro-controller to send low date rate packets via radio modules that operating on 868 MHz band to achieve long distance. We embed and implement the design with low cost and low power components and send real-time services such as GPS co-ordinates, temperature, image and etc. over wireless network. We develop our design to relay the transmission from the source node to the destination node by using a forwarding node in between them to achieve further distance. The designed schemes responds to each other with acknowledgements assuring the successful transactions between the nodes and for the request of retransmissions. 
>
> The schemes are developed further, where the retransmission will occur with an adaptive data rate at the end of the transmission process to ensure the successful transfer of a large file. The data are collected and examined on the sender and receiver ends and calculate parameters such as packet loss rate, transmission delay, etc., by running various tests in different scenarios and optimising the experimental results.
>
> [Link to PDF](/links/master_thesis-2016.pdf)