+++
date = "2021-11-03"
title = "Convert the Linux sub-system WSL version from 1 to version 2"
slug = "post-12"
authors = ["Samiul Saki"]
categories = ["Tutorial","Post"]
tags = ["Windows"]
+++

In this guide we are going to change the WSL to any versions based on requirements. In this demo we are going to upgrade WSL version 1 to 2.

## Prerequisties:
- Windows 10 build 18917 or higher
- Powershell (with Administrator access)
- Linux sub-system for Windows (with any distro)

#### Steps
- We are going to enableLinux sub-system in Windows machine if this is already not enabled. Open Powershell as Administrator and run this command in the shell:

    ```powershell
    > Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux

    Output:
    
    Path          :
    Online        : True
    RestartNeeded : False
    ```

- Then you need to run this:

    ```powershell
    > Enable-WindowsOptionalFeature -Online -FeatureName VirtualMachinePlatform
    ```
- Now you can upgrade/change WSL version of your distro. In this demo we are going to use Ubuntu-20.04 as distro, but you can use any distro you would prefer ([how to](https://www.windowscentral.com/install-windows-subsystem-linux-windows-10#install_linux_subsystem_powershell_windows10)).
    - If you already have a sub-system isntalled on your Windows machine and you would like to upgrade the version of that particular system you can do that as well. Find your available Linux sub-system distro list in verbose mode:
    ```powershell
    > wsl -l -v
    
    Output:
      NAME                   STATE           VERSION
    * Ubuntu-20.04           Running         1
      docker-desktop-data    Running         2
      docker-desktop         Running         2

    ```
- Now run the following command in Powershell to set the WSL version of the distro to version 2
    ```powershell
    > wsl --set-version <Distro> 2    
    
    Ex:
    > wsl --set-version Ubuntu-20.04 2
    The operation completed successfully.
    ```
    > This might take  few minutes before its completed, size of the distro.
- If you don't run the following command the WSL version for all the future distro will be installed as version 1. Lets change it:
    ```powershell
    > wsl --set-default-version 2
    ```
- Run the list command to see if this worked:
    ```powershell
    > wsl --list --verbose
    ```


> You might loose the internet connection on Linux sub-system once you upgrade it to WSL v.2. Do the following setup to fix the issue:

- Remove the `resolv.conf` file in the Linux sub-system:
    ```console
    # sudo rm /etc/resolv.conf
    ```
- Now add this to the resolv.conf file:
    ```console
    # sudo bash -c 'echo "nameserver 1.1.1.1" > /etc/resolv.conf'
    ```
- Now run these following lines to add to the `wsl.conf` file in order to make WSL make changes to the resolv.conf file in the future:
    ```console
    sudo bash -c 'echo "[network]" > /etc/wsl.conf'
    sudo bash -c 'echo "generateResolvConf = false" >> /etc/wsl.conf'
    ```
- To make the resolv.conf file immutable to changes and deletion, even by superuser (in our demo WSL):
    ```console
    sudo chattr +i /etc/resolv.conf
    ```

- Test the connectivity:
    ```console
    # nslookup google.com
    Server:         1.1.1.1
    Address:        1.1.1.1#53

    Non-authoritative answer:
    Name:   google.com
    Address: 142.250.74.14
    Name:   google.com
    Address: 2a00:1450:400f:802::200e
    ```


### Voila! You are done.

#### Note: You might also make it work by just simply uninstalling **ufw** from Linux sub-system instead of making changes on resolv.conf. Do a restart after.