+++
date = "2018-05-20"
title = "Autonomous Virtual Machines for Ensuring QoS of Distributed Web Services Using Evolutionary Game Theory"
slug = "post-5"
authors = ["Samiul Saki"]
categories = ["Thesis"]
tags = ["Cloud"]
+++

### Masters Thesis - A proof of concept of self-organising virtual machines
> ![FWNGX1.md.png](/links/infrastructure.png)
>
> The virtualisation of hardware resources is one of the universal features of modern-day cloud computing. The agility and effectiveness of hardware resource pool virtualisation,  geographic diversity and universal connectivity of various inter-connected components play a vital role in cloud computing. The research of this field is now magnified towards the study and development of secure and stable migration of the shared virtual systems. Efficient usage of physical components by sharing over a public space is now one of the main focus of the traditional and upcoming cloud providers and the enterprise networks.
>
> It is already in practice to clone virtual machines (VMs) in order to create a more scalable and flexible infrastructure over the internet. Popular technique such as migration of VMs while maintaining the optimal quality of service (QoS) of the distributed web services across the globe has also become an on-demand feature of cloud infrastructure. Administration of these virtual systems with various types of services are now needed to be more flexible towards adapting to the clients’ requests. The broad demand for these services in large-scale requires some sort of autonomy in the system as it is becoming more complicated to maintain them manually. The other feasible options, that are mostly adopted by data centres, are to implement a centralised system which controls all the VMs in the cloud. This solution is adequate until the backbone of the central system breaks down or cannot recover from a node depletion. The concept of a self-aware VM is to perform tasks independently, self-regulate its behaviour to maintain stability by analysing the network condition and other external/internal attributes in order to introduce an equivalence in the cloud architecture. This phenomenon of virtualisation demands an effective algorithm that can conduct and administer the emergence behaviour of the virtualised systems. This  designed algorithm need to approach an autonomy with self-governance and self-maintenance feature in the network to maintain system-wide stability.
>
> In this project, we propose different variants of migration algorithms by adopting the evolutionary game theory, to achieve the emergence behaviour of the VMs in a network by equalising the average response times of the entire connected network. Our algorithms are applied to the VMs in the network, to make them be more independent and autonomous. Using these algorithms each individual VM can observe and learn the network condition at a particular moment, implement the algorithm, analyse and select the affected data-centres as the migrating destination, whilst maintaining the quality of service of the distributed web services. These algorithms are designed to stabilise the average response time in all the adjoined data-centres in a network by distributing the idle migrating VMs among each other without any requirement of a centralised management.
>
> Furthermore, we perform multiple experiments by implementing these algorithms in simulations and in real-life cloud infrastructure test-bed and accumulate results of our design performance of modified self-managed and self-coordinated VMs. We analyse and discuss the performance of the algorithms in VM instances deployed in the cloud platform. The results show, using our migration algorithms, we can equalise the response time in the entire network with the self-organised VMs, by applying an autonomy in the system, while providing a satisfactory QoS of distributed web service.
>
> [Link to PDF](/links/masters_thesis_2018.pdf)
