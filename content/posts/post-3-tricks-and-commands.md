+++
date = "2021-10-13"
title = "Useful Commands (updated occationally)"
slug = "post-3"
authors = ["Samiul Saki"]
categories = ["Tips"]
tags = ["Linux/Unix","Bash"]
+++

I got tired of forgetting simplest of commands all the time and end up googling for it. So here is a copy of my dumps.

- [1 Passwords](#1-passwords)
    - [1.1 Linux Set or Change User Password](#11-linux-set-or-change-user-password)
    - [1.2 Linux change password for other user account](#12-linux-change-password-for-other-user-account)
    - [1.3 Linux Change Group Password](#13-linux-change-group-password)
    - [1.4 Forcing Linux user to change password at their next login](#14-forcing-linux-user-to-change-password-at-their-next-login)
    - [1.5 Locking and Unlocking user password of the named account](#15-locking-and-unlocking-user-password-of-the-named-account)    
- [2 Add and Delete Users on Ubuntu](#2-add-and-delete-users-on-ubuntu)
- [3 Some Useful Aliases and Functions](#3-some-useful-aliases-and-functions)
    - [3.1 Handy Aliases](#31-handy-aliases)
    - [3.2 Functions and User Configurations](#32-functions-and-user-configurations)

## 1 Passwords
Passwords are encrypted. Usually in these files. Can use cat or grep:
``` console
$ cat /etc/passwd
$ grep '^usernamehere' /etc/passwd
```
#### 1.1 Linux Set or Change User Password
Must use:
- Lower case alphabetics
- Upper case alphabetics
- Digits 0 thru 9
- Punctuation marks/spacial characters
``` console
$ passwd

Changing password for {username}
(current) UNIX password:
Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully
```

#### 1.2 Linux change password for other user account
``` console
$ sudo passwd {other_username}

Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully
```

#### 1.3 Linux Change Group Password
``` console
$ sudo passwd -g sales
```
#### 1.4 Forcing Linux user to change password at their next login
We can force users to change their password the next time they log in via GUI or CLI methods. 
``` console
$ sudo passwd -e {username}
$ sudo passwd --expire {username}
```
Let us immediately expire an account’s password
``` console
$ sudo passwd -e username

passwd: password expiry information changed.
```

#### 1.5 Locking and Unlocking user password of the named account
Users with a locked password are not allowed to change their password. To lock the user with password
``` console
$ sudo passwd -l {username}
```
To unlock the user with password
``` cosnole
$ sudo passwd -u {username}
```


## 2. Add and Delete Users on Ubuntu
Add a user:
``` cosnole
$ sudo adduser newuser

Set password prompts:
Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully

User information prompts:
Changing the user information for username
Enter the new value, or press ENTER for the default
    Full Name []:
    Room Number []:
    Work Phone []:
    Home Phone []:
    Other []:
Is the information correct? [Y/n]

$ sudo groups newuser

Output
newuser : newuser

$ sudo usermod -aG sudo newuser
$ su - newuser
$ sudo some_command
$ cat /etc/sudoers

root    ALL=(ALL:ALL) ALL
newuser ALL=(ALL:ALL) ALL # ADD THIS LINE
```

Delete a user:
``` console
$ sudo deluser newuser
or
$ sudo deluser --remove-home newuser
$ cat /etc/sudoers

root    ALL=(ALL:ALL) ALL
newuser ALL=(ALL:ALL) ALL   # DELETE THIS LINE
```

## 3. Some Useful Aliases and Functions
Here is some of the aliases I have in my ~/.bashrc. Use and adjust the aliases or functions that works for you best.


#### 3.1 Handy Aliases
{{< detail-tag "CLICK TO EXPAND" >}}
``` console
alias -='cd -'                                                                                                                             
alias ...=../..                                                                                                                            
alias ....=../../..                                                                                                                        
alias .....=../../../..                                                                                                                    
alias ......=../../../../..                                                                                                                
alias ..q=exit                                                                                                                             
alias .src='source ~/.bashrc'                                                                                                               
alias 1='cd -'                                                                                                                             
alias 2='cd -2'                                                                                                                            
alias 3='cd -3'                                                                                                                            
alias 4='cd -4'                                                                                                                            
alias 5='cd -5'                                                                                                                            
alias 6='cd -6'                                                                                                                            
alias 7='cd -7'                                                                                                                            
alias 8='cd -8'                                                                                                                            
alias 9='cd -9'                                                                                                                            
alias _='sudo '                                                                                                                            
alias afind='ack -il'                                                                                                                      
alias c.='code . > /dev/null'                                                                                                              
alias cpu5='ps auxf | sort -nr -k 3 | head -5'                                                                                             
alias diff='diff --color'                                                                                                                  
alias e.='explorer.exe . || true'                                                                                                          
alias egrep='egrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,.idea,.tox}'                                                         
alias extip='curl icanhazip.com'                                                                                                           
alias fgrep='fgrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,.idea,.tox}'
# some git command shortcuts
alias g=git                                                                                                                                
alias ga='git add'                                                                                                                         
alias gaa='git add --all'                                                                                                                  
alias gam='git am'                                                                                                                         
alias gama='git am --abort'                                                                                                                
alias gamc='git am --continue'                                                                                                             
alias gams='git am --skip'                                                                                                                 
alias gamscp='git am --show-current-patch'                                                                                                 
alias gap='git apply'                                                                                                                      
alias gapa='git add --patch'                                                                                                               
alias gapt='git apply --3way'                                                                                                              
alias gau='git add --update'                                                                                                               
alias gav='git add --verbose'                                                                                                              
alias gb='git branch'                                                                                                                      
alias gbD='git branch -D'                                                                                                                  
alias gba='git branch -a'                                                                                                                  
alias gbd='git branch -d'                                                                                                                  
alias gbda='git branch --no-color --merged | command grep -vE "^([+*]|\s*($(git_main_branch)|$(git_develop_branch))\s*$)" | command xargs g
alias gbl='git blame -b -w'                                                                                                                
alias gbnm='git branch --no-merged'                                                                                                        
alias gbr='git branch --remote'                                                                                                            
alias gbs='git bisect'                                                                                                                     
alias gbsb='git bisect bad'                                                                                                                
alias gbsg='git bisect good'                                                                                                               
alias gbsr='git bisect reset'                                                                                                              
alias gbss='git bisect start'                                                                                                              
alias gc='git commit -v'                                                                                                                   
alias 'gc!'='git commit -v --amend'                                                                                                        
alias gca='git commit -v -a'                                                                                                               
alias 'gca!'='git commit -v -a --amend'                                                                                                    
alias gcam='git commit -a -m'                                                                                                              
alias 'gcan!'='git commit -v -a --no-edit --amend'                                                                                         
alias 'gcans!'='git commit -v -a -s --no-edit --amend'                                                                                     
alias gcas='git commit -a -s'                                                                                                              
alias gcasm='git commit -a -s -m'                                                                                                          
alias gcb='git checkout -b'                                                                                                                
alias gcd='git checkout $(git_develop_branch)'                                                                                             
alias gcf='git config --list'                                                                                                              
alias gcl='git clone --recurse-submodules'                                                                                                 
alias gclean='git clean -id'                                                                                                               
alias gcm='git checkout $(git_main_branch)'                                                                                                
alias gcmsg='git commit -m'                                                                                                                
alias 'gcn!'='git commit -v --no-edit --amend'                                                                                             
alias gco='git checkout'                                                                                                                   
alias gcor='git checkout --recurse-submodules'                                                                                             
alias gcount='git shortlog -sn'                                                                                                            
alias gcp='git cherry-pick'                                                                                                                
alias gcpa='git cherry-pick --abort'                                                                                                       
alias gcpc='git cherry-pick --continue'                                                                                                    
alias gcs='git commit -S'                                                                                                                  
alias gcsm='git commit -s -m'                                                                                                              
alias gcss='git commit -S -s'                                                                                                              
alias gcssm='git commit -S -s -m'                                                                                                          
alias gd='git diff'                                                                                                                        
alias gdca='git diff --cached'                                                                                                             
alias gdct='git describe --tags $(git rev-list --tags --max-count=1)'                                                                      
alias gdcw='git diff --cached --word-diff'                                                                                                 
alias gds='git diff --staged'                                                                                                              
alias gdt='git diff-tree --no-commit-id --name-only -r'                                                                                    
alias gdup='git diff @{upstream}'                                                                                                          
alias gdw='git diff --word-diff'                                                                                                           
alias gf='git fetch'                                                                                                                       
alias gfa='git fetch --all --prune --jobs=10'                                                                                              
alias gfg='git ls-files | grep'                                                                                                            
alias gfo='git fetch origin'                                                                                                               
alias gg='git gui citool'                                                                                                                  
alias gga='git gui citool --amend'                                                                                                         
alias ggpull='git pull origin "$(git_current_branch)"'                                                                                     
alias ggpur=ggu                                                                                                                            
alias ggpush='git push origin "$(git_current_branch)"'                                                                                     
alias ggsup='git branch --set-upstream-to=origin/$(git_current_branch)'                                                                    
alias ghh='git help'                                                                                                                       
alias gignore='git update-index --assume-unchanged'                                                                                        
alias gignored='git ls-files -v | grep "^[[:lower:]]"'                                                                                     
alias git-svn-dcommit-push='git svn dcommit && git push github $(git_main_branch):svntrunk'                                                
alias gk='\gitk --all --branches &!'                                                                                                       
alias gke='\gitk --all $(git log -g --pretty=%h) &!'                                                                                       
alias gl='git pull'                                                                                                                        
alias glg='git log --stat'                                                                                                                 
alias glgg='git log --graph'                                                                                                               
alias glgga='git log --graph --decorate --all'                                                                                             
alias glgm='git log --graph --max-count=10'                                                                                                
alias glgp='git log --stat -p'                                                                                                             
alias glo='git log --oneline --decorate'                                                                                                   
alias globurl='noglob urlglobber '                                                                                                         
alias glod='git log --graph --pretty='\''%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset'\'                    
alias glods='git log --graph --pretty='\''%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset'\'' --date=short'    
alias glog='git log --oneline --decorate --graph'                                                                                          
alias gloga='git log --oneline --decorate --graph --all'                                                                                   
alias glol='git log --graph --pretty='\''%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset'\'                    
alias glola='git log --graph --pretty='\''%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset'\'' --all'           
alias glols='git log --graph --pretty='\''%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset'\'' --stat'          
alias glp=_git_log_prettily                                                                                                                
alias glum='git pull upstream $(git_main_branch)'                                                                                          
alias gm='git merge'                                                                                                                       
alias gma='git merge --abort'                                                                                                              
alias gmom='git merge origin/$(git_main_branch)'                                                                                           
alias gmtl='git mergetool --no-prompt'                                                                                                     
alias gmtlvim='git mergetool --no-prompt --tool=vimdiff'                                                                                   
alias gmum='git merge upstream/$(git_main_branch)'                                                                                         
alias gp='git push'                                                                                                                        
alias gpd='git push --dry-run'                                                                                                             
alias gpf='git push --force-with-lease'                                                                                                    
alias 'gpf!'='git push --force'                                                                                                            
alias gpoat='git push origin --all && git push origin --tags'                                                                              
alias gpr='git pull --rebase'                                                                                                              
alias gpristine='git reset --hard && git clean -dffx'                                                                                      
alias gpsup='git push --set-upstream origin $(git_current_branch)'                                                                         
alias gpu='git push upstream'                                                                                                              
alias gpull='git pull origin master'                                                                                                       
alias gpush='git push origin master'                                                                                                       
alias gpv='git push -v'                                                                                                                    
alias gr='git remote'                                                                                                                      
alias gra='git remote add'                                                                                                                 
alias grb='git rebase'                                                                                                                     
alias grba='git rebase --abort'                                                                                                            
alias grbc='git rebase --continue'                                                                                                         
alias grbd='git rebase $(git_develop_branch)'                                                                                              
alias grbi='git rebase -i'                                                                                                                 
alias grbm='git rebase $(git_main_branch)'                                                                                                 
alias grbo='git rebase --onto'                                                                                                             
alias grbs='git rebase --skip'                                                                                                             
alias grep='grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,.idea,.tox}'                                                           
alias grev='git revert'                                                                                                                    
alias grh='git reset'                                                                                                                      
alias grhh='git reset --hard'                                                                                                              
alias grm='git rm'                                                                                                                         
alias grmc='git rm --cached'                                                                                                               
alias grmv='git remote rename'                                                                                                             
alias groh='git reset origin/$(git_current_branch) --hard'                                                                                 
alias grrm='git remote remove'                                                                                                             
alias grs='git restore'                                                                                                                    
alias grset='git remote set-url'                                                                                                           
alias grss='git restore --source'                                                                                                          
alias grst='git restore --staged'                                                                                                          
alias grt='cd "$(git rev-parse --show-toplevel || echo .)"'                                                                                
alias gru='git reset --'                                                                                                                   
alias grup='git remote update'                                                                                                             
alias grv='git remote -v'                                                                                                                  
alias gsb='git status -sb'                                                                                                                 
alias gsd='git svn dcommit'                                                                                                                
alias gsh='git show'                                                                                                                       
alias gsi='git submodule init'                                                                                                             
alias gsps='git show --pretty=short --show-signature'                                                                                      
alias gsr='git svn rebase'                                                                                                                 
alias gss='git status -s'                                                                                                                  
alias gst='git status'                                                                                                                     
alias gsta='git stash push'                                                                                                                
alias gstaa='git stash apply'                                                                                                              
alias gstall='git stash --all'                                                                                                             
alias gstat='git status'                                                                                                                   
alias gstc='git stash clear'                                                                                                               
alias gstd='git stash drop'                                                                                                                
alias gstl='git stash list'                                                                                                                
alias gstp='git stash pop'                                                                                                                 
alias gsts='git stash show --text'                                                                                                         
alias gstu='gsta --include-untracked'                                                                                                      
alias gsu='git submodule update'                                                                                                           
alias gsw='git switch'                                                                                                                     
alias gswc='git switch -c'                                                                                                                 
alias gswd='git switch $(git_develop_branch)'                                                                                              
alias gswm='git switch $(git_main_branch)'                                                                                                 
alias gtl='gtl(){ git tag --sort=-v:refname -n -l "${1}*" }; noglob gtl'                                                                   
alias gts='git tag -s'                                                                                                                     
alias gtv='git tag | sort -V'                                                                                                              
alias gunignore='git update-index --no-assume-unchanged'                                                                                   
alias gunwip='git log -n 1 | grep -q -c "\-\-wip\-\-" && git reset HEAD~1'                                                                 
alias gup='git pull --rebase'                                                                                                              
alias gupa='git pull --rebase --autostash'                                                                                                 
alias gupav='git pull --rebase --autostash -v'                                                                                             
alias gupv='git pull --rebase -v'                                                                                                          
alias gwch='git whatchanged -p --abbrev-commit --pretty=medium'                                                                            
alias gwip='git add -A; git rm $(git ls-files --deleted) 2> /dev/null; git commit --no-verify --no-gpg-sign -m "--wip-- [skip ci]"'
alias history=omz_history                                                                                                      
alias l='ls -lah'                                                                                                                          
alias la='ls -lAh'                                                                                                                         
alias ll='ls -FglAhp'                                                                                                                      
alias lmount='mount | column -t'                                                                                                           
alias ls='ls --color=tty'                                                                                                                  
alias lsa='ls -lah'                                                                                                                        
alias md='mkdir -p'                                                                                                                        
alias mem5='ps auxf | sort -nr -k 4 | head -5'                                                                                             
alias rd=rmdir                                                                                                                             
alias speedtest='curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python3 -'                            
alias which-command=whence
alias ..q="exit"                                                                                                                                                                                     
alias speedtest='curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python3 -'                                                                                      
alias mem5="ps auxf | sort -nr -k 4 | head -5"                                                                                                                                                       
alias cpu5="ps auxf | sort -nr -k 3 | head -5"                                                                                                                                                       
alias extip="curl icanhazip.com"                                                                                                                                                                     
alias lmount="mount | column -t"                                                                                                                                                                     
alias e.="explorer.exe . || true"
alias c.="code . > /dev/null"
alias ports='netstat -tulanp'
# stop after sending count ECHO_REQUEST packets
alias ping='ping -c 5' 
# do not wait interval 1 second, go fast
alias fastping='ping -c 100 -s.2'
# replace mac with your actual server mac address
alias wakeupnas01='/usr/bin/wakeonlan 00:11:32:11:15:FC'
alias wakeupnas02='/usr/bin/wakeonlan 00:11:32:11:15:FD'
alias wakeupnas03='/usr/bin/wakeonlan 00:11:32:11:15:FE'
# shortcut  for iptables and pass it via sudo
alias ipt='sudo /sbin/iptables'
# display all rules
alias iptlist='sudo /sbin/iptables -L -n -v --line-numbers'
alias iptlistin='sudo /sbin/iptables -L INPUT -n -v --line-numbers'
alias iptlistout='sudo /sbin/iptables -L OUTPUT -n -v --line-numbers'
alias iptlistfw='sudo /sbin/iptables -L FORWARD -n -v --line-numbers'
alias firewall=iptlist
# get web server headers
alias header='curl -I'
# find out if remote server supports gzip / mod_deflate or not
alias headerc='curl -I --compress'
# do not delete / or prompt if deleting more than 3 files at a time
alias rm='rm -I --preserve-root'
# confirmation
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'
# Parenting changing perms on / 
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'
# distro specific  - Debian / Ubuntu install with apt-get
alias apt-get="sudo apt-get"
alias updatey="sudo apt-get --yes"
# update on one command
alias update='sudo apt-get update && sudo apt-get upgrade'
# become root
alias root='sudo -i'
alias su='sudo -i'
# reboot / halt / poweroff
alias reboot='sudo /sbin/reboot'
alias poweroff='sudo /sbin/poweroff'
alias halt='sudo /sbin/halt'
alias shutdown='sudo /sbin/shutdown'
# also pass it via sudo so whoever is admin can reload it without calling you
alias nginxreload='sudo /usr/local/nginx/sbin/nginx -s reload'
alias nginxtest='sudo /usr/local/nginx/sbin/nginx -t'
alias lightyload='sudo /etc/init.d/lighttpd reload'
alias lightytest='sudo /usr/sbin/lighttpd -f /etc/lighttpd/lighttpd.conf -t'
alias httpdreload='sudo /usr/sbin/apachectl -k graceful'
alias httpdtest='sudo /usr/sbin/apachectl -t && /usr/sbin/apachectl -t -D DUMP_VHOSTS'
# backup scripts
alias backup='sudo /home/scripts/admin/scripts/backup/wrapper.backup.sh --type local --taget /raid1/backups'
alias nasbackup='sudo /home/scripts/admin/scripts/backup/wrapper.backup.sh --type nas --target nas01'
alias s3backup='sudo /home/scripts/admin/scripts/backup/wrapper.backup.sh --type nas --target nas01 --auth /home/scripts/admin/.authdata/amazon.keys'
alias rsnapshothourly='sudo /home/scripts/admin/scripts/backup/wrapper.rsnapshot.sh --type remote --target nas03 --auth /home/scripts/admin/.authdata/ssh.keys --config /home/scripts/admin/scripts/backup/config/adsl.conf'
alias rsnapshotdaily='sudo  /home/scripts/admin/scripts/backup/wrapper.rsnapshot.sh --type remote --target nas03 --auth /home/scripts/admin/.authdata/ssh.keys  --config /home/scripts/admin/scripts/backup/config/adsl.conf'
alias rsnapshotweekly='sudo /home/scripts/admin/scripts/backup/wrapper.rsnapshot.sh --type remote --target nas03 --auth /home/scripts/admin/.authdata/ssh.keys  --config /home/scripts/admin/scripts/backup/config/adsl.conf'
alias rsnapshotmonthly='sudo /home/scripts/admin/scripts/backup/wrapper.rsnapshot.sh --type remote --target nas03 --auth /home/scripts/admin/.authdata/ssh.keys  --config /home/scripts/admin/scripts/backup/config/adsl.conf'
alias amazonbackup=s3backup
# all of our servers eth1 is connected to the Internets via vlan / router etc
alias dnstop='dnstop -l 5  eth1'
alias vnstat='vnstat -i eth1'
alias iftop='iftop -i eth1'
alias tcpdump='tcpdump -i eth1'
alias ethtool='ethtool eth1'
# work on wlan0 by default
# only useful for laptop as all servers are without wireless interface
alias iwconfig='iwconfig wlan0'
## pass options to free
alias meminfo='free -m -l -t'
## get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
# get top process eating cpu
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'
# get server cpu info 
alias cpuinfo='lscpu'
# older system use /proc/cpuinfo
alias cpuinfo='less /proc/cpuinfo'
# get GPU ram on desktop / laptop
alias gpumeminfo='grep -i --color memory /var/log/Xorg.0.log'
# Reboot my home Linksys WAG160N / WAG54 / WAG320 / WAG120N Router / Gateway from *nix.
alias rebootlinksys="curl -u 'admin:my-super-password' 'http://192.168.1.2/setup.cgi?todo=reboot'"
# Reboot tomato based Asus NT16 wireless bridge
alias reboottomato="ssh admin@192.168.1.1 /sbin/reboot"
# this ones saved by butt so many times
alias wget='wget -c'
alias ff4='/opt/firefox4/firefox'
alias ff13='/opt/firefox13/firefox'
alias chrome='/opt/google/chrome/chrome'
alias opera='/opt/opera/opera'
alias ff=ff13
alias browser=chrome
# set some other defaults
alias df='df -H'
alias du='du -ch'
# top is atop, just like vi is vim
alias top='atop'
# nfsrestart  - must be root 
# refresh nfs mount / cache etc for Apache
alias nfsrestart='sync && sleep 2 && /etc/init.d/httpd stop && umount netapp2:/exports/http && sleep 2 && mount -o rw,sync,rsize=32768,wsize=32768,intr,hard,proto=tcp,fsc natapp2:/exports /http/var/www/html &&  /etc/init.d/httpd start'
# memcached
alias mcdstats='/usr/bin/memcached-tool 10.10.27.11:11211 stats'
alias mcdshow='/usr/bin/memcached-tool 10.10.27.11:11211 display'
alias flushmcd='echo "flush_all" | nc 10.10.27.11 11211'
alias ipe='curl ipinfo.io/ip'
alias ipi='ipconfig getifaddr en0'
```
{{< /detail-tag >}}

#### 3.2 Functions and User Configurations
{{< detail-tag "CLICK TO EXPAND" >}}
```console
# if user is not root, pass all commands via sudo
if [ $UID -ne 0 ]; then
    alias reboot='sudo reboot'
    alias update='sudo apt-get upgrade'
fi

# Get os name via uname
_myos="$(uname)"
 
# add alias as per os using $_myos
case $_myos in
   Linux) alias foo='/path/to/linux/bin/foo';;
   FreeBSD|OpenBSD) alias foo='/path/to/bsd/bin/foo' ;;
   SunOS) alias foo='/path/to/sunos/bin/foo' ;;
   *) ;;
esac

gcomm() {                                                                                                                                                                                            
        git commit -am "$1 @ $(date)"                                                                                                                                                                 
}
                                                                                                                                                                                       
weather() {                                                                                                                                                                                          
        clear                                                                                                                                                                                        
        curl wttr.in/$1
}                                                                                                                                                                                                    

cht () {                                                                                                                                                                                             
        curl cheat.sh/$1                                                                                                                                                                             
}

find_ip () {                                                                                                                                                                                         
        IP=$1                                                                                                                                                                                        
        MASK=$2                                                                                                                                                                                      
        echo $IP $MASK                                                                                                                                                                             
        sudo nmap -PR $IP/$MASK -sn; arp -a | grep -v 'incomplete'                                                                                                                                   
}

# coloring and stuff                                                                                                                                                                                 
export HISTSIZE=10000                                                                                                                                                                                
export PATH=~/bin:$PATH                                                                                                                                                                              
                                                                                                                                                                                                     
# set colors for LS_COLORS
eval `dircolors ~/.dircolors`                                                                                                                                                                        
                                                                                                                                                                                                     
# hide the user and computer name in our prompt                                                                                                                                                      
DEFAULT_USER=$USER                                                                                                                                                                                   
                                                                                                                                                                        
# Function to delegate to Windows to open each of its arguments                                                                                                                                      
start() {
    for file in "$@"
    do                                                                                                                                                 
        cmd.exe /C "$file"
    done
}
```
{{< /detail-tag >}}


more here soon 


###### Ref: 
1. https://www.cyberciti.biz/tips/bash-aliases-mac-centos-linux-unix.html