+++
date = "2017-08-02"
title = "A Dynamically Scaled Cloud-based Web-Service using Docker-Swarm, HAProxy and OpenStack"
slug = "post-6"
authors = ["Samiul Saki"]
categories = ["Project"]
tags = ["Cloud", "DevOps"]
+++

### Project
> ![FWOdkQ.md.webp](/links/cluster_openstack.jpg)
>
> In this report, a cluster of cloud-based web-service is built in order to adjust the number of Docker-based web-servers (containers) in order to manage the incoming rate of user requests. The main focus of this cloud-based web-services is to prevent service depletion due to heavy traffic from outside network and automate the nodes to generate new services to adapt the requests approaching for accessing those web-services. To achieve this infrastructure, several tools such as Docker-Swarm, Docker-Machine, HAProxy have been used. Additionally, Grafana, InfluxDB and cAdvisor have been used to monitor the traffic pattern as well as CPU, memory and file-system usage of the nodes. 
>
> Upon successful testing with the ApacheBench to exhaust the traffic handling capability of the load-balancer, the services are auto-scaled in order to adjust the concurrent connection requests and to stabilize the network overload.
>
> [Link to PDF](/links/project_08_2017.pdf)
