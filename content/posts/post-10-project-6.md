+++
date = "2015-11-20"
title = "MAC Enhancement and Performance Evaluation in Wireless Sensor Networks with Energy Harvesting"
slug = "post-10"
authors = ["Samiul Saki"]
categories = ["Project"]
tags = ["CSMA/CA MAC Protocol", "Wireless Sensor Network"]
+++

### Project
> Wireless sensor nodes are usually battery equipped and designed to maximise the energy conservation in order to avoid as much as battery extortion. Considering the limited amount of energy available in sensor nodes, proper distribution of harvested energy is appear to be most promising technology in wireless sensor networks (WSN). Harvested energy arrival rate cannot be predicted in advance due to its stochastic behaviour. This demands the necessity of designing an efficient MAC protocol for WSNs. In this project, we design four energy harvesting aware backoff (BO) algorithm for CSMA/CA and duty cycle scheme to extend the lifetime of sensor nodes by administering energy harvesting rate as well as residual energy. 
>  
> We enhanced backoff algorithm with energy harvesting wireless nodes using CSMA/CA MAC mechanism to extend the lifetime of overall sensor networks. In addition, we evaluate the duty cycle mechanism by comparing lifetime of three different schemes considering energy harvesting rate as a control parameter. The simulation results shows that MAC mechanism with energy harvesting awareness enhance the performance of WSNs. Our numerical results shows EH and residual energy based backoff algorithm for CSMA/CA enhances the lifetime of WSNs.