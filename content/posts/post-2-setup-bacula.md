+++
date = "2018-03-11"
title = "Setup Bacula in Ubuntu"
slug = "post-2"
authors = ["Samiul Saki"]
categories = ["Tutorial","Post"]
tags = ["Linux/Unix","Bash"]
+++

In this guide I will go though the setup process of Bacula server and client in a ubuntu system. It should work with any ubuntu distro. The guide has two parts:
- [**Bacula Server Setup**](#bacula-server) 
- [**Bacula Client Setup**](#bacula-client)

--------
## Bacula-Server:
```console
$ sudo apt-get update
$ sudo apt-get install mysql-server
``` 
> Enter a Password (remember the password)
``` console
$ sudo apt-get install bacula-server bacula-client
```
- Select YES for configuring database
- Enter mysql password (from previous step)
- Enter a new password and confirm

``` console
$ sudo chmod 755 /etc/bacula/scripts/delete_catalog_backup
```
### Mount the volume:
``` console
mkfs.ext4 /dev/vdb
mount /dev/vdb /mnt
mkdir -p /mnt/bacula/backup /mnt/bacula/restore
$ sudo chown -R bacula:bacula /mnt/bacula
$ sudo chmod -R 700 /mnt/bacula
```

### Configure Bacula Director:
``` console
nano /etc/bacula/bacula-dir.conf
```
> Find the Job resource with a name of "BackupClient" (ca line 45) and rename the Name part:
``` MakeFile
Job {
    Name = "BackupLocalFiles"
    JobDefs = "DefaultJob"
}
```
> Find the Job resource that is named "RestoreFiles" (ca line 77) and rename the Name and Where part:
``` Makefile
Job {
    Name = "RestoreLocalFiles"
    Type = Restore
    Client=backup-sam-fd
    FileSet="Full Set"
    Storage = File
    Pool = Default
    Messages = Standard
    Where = /mnt/bacula/restore
}
```
### Configure File Set: (ca line 90)
``` Makefile
FileSet {
    Name = "Full Set"
    Include {
        Options {
        signature = MD5
        compression = GZIP
        }
    File = /
    }
    Exclude {
            File = /var/lib/bacula
            File = /mnt/bacula
        File = /proc
            File = /tmp
            File = /.journal
            File = /.fsck
    }
}
```
### Configure Storage Daemon Connection: 
> Use the backup server FQDN here (ca line 185):
``` Makefile
Storage {
    Name = File
    # Do not use "localhost" here
    Address = backup-sam.openstacklocal                # N.B. Use a fully qualified name here
    SDPort = 9103
    Password = "tL3-S6OfmJhkT6CGN9axDWm-64uc18oko"
    Device = FileStorage
    Media Type = File
}
```
### Configure Pool:
> Add a line that specifies a Label Format (ca line 282):
``` makefile
Pool {
    Name = File
    Pool Type = Backup
    Label Format = Local-
    Recycle = yes                       # Bacula can automatically recycle Volumes
    AutoPrune = yes                     # Prune expired volumes
    Volume Retention = 365 days         # one year
    Maximum Volume Bytes = 50G          # Limit Volume size to something reasonable
    Maximum Volumes = 100               # Limit number of Volumes in Pool
}
```
### Check Director Configuration:
``` console
bacula-dir -tc /etc/bacula/bacula-dir.conf
```
### Configure Storage Daemon:
``` console
nano /etc/bacula/bacula-sd.conf
```
(ca line 13):
``` Makefile
Storage {                             # definition of myself
    Name = backup-sam-sd
    SDPort = 9103                  # Director's port
    WorkingDirectory = "/var/lib/bacula"
    Pid Directory = "/var/run/bacula"
    Maximum Concurrent Jobs = 20
    SDAddress = backup-sam.openstacklocal
}
```
(ca line 53):
``` Makefile
Device {
    Name = FileStorage
    Media Type = File
    Archive Device = /mnt/bacula/backup
    LabelMedia = yes;                   # lets Bacula label unlabeled media
    Random Access = Yes;
    AutomaticMount = yes;               # when device opened, read it
    RemovableMedia = no;
    AlwaysOpen = no;
}
```
### Verify Storage Daemon Configuration:
``` console
bacula-sd -tc /etc/bacula/bacula-sd.conf
```
### Restart Bacula Director and Storage Daemon:
``` console
$ sudo service bacula-director restart
$ sudo service bacula-sd restart
```
### To backup:
``` console
$ sudo bconsole

run -> 1 -> yes
```
### To restore:
``` console
$ sudo bconsole

restore all -> 5 -> 2
```

### Delete Restored Files
> You may want to delete the restored files to free up disk space. To do so, use this command:
``` console
sudo -u root bash -c "rm -rf /bacula/restore/*"
```

--------
## Bacula-Client:

### On Bacula-Server (addition to the prior setup):
``` console
sudo mkdir /etc/bacula/conf.d
sudo nano /etc/bacula/bacula-dir.conf
```
> At the end of the file add, this line:
``` Makefile
@|"find /etc/bacula/conf.d -name '*.conf' -type f -exec echo @{} \;"
```
> Save and Exit.
``` console
sudo nano /etc/bacula/conf.d/pools.conf
```
Add this as content:
``` Makefile
Pool {
        Name = RemoteFile
        Pool Type = Backup
        Label Format = Remote-
        Recycle = yes                       # Bacula can automatically recycle Volumes
        AutoPrune = yes                     # Prune expired volumes
        Volume Retention = 365 days         # one year
        Maximum Volume Bytes = 50G          # Limit Volume size to something reasonable
        Maximum Volumes = 100               # Limit number of Volumes in Pool
    }
```
### Check for errors:
``` console
sudo bacula-dir -tc /etc/bacula/bacula-dir.conf
```

### Install Bacula-Client:
``` console
sudo apt-get update
```
> Then install the bacula-client package:
``` console
sudo apt-get install bacula-client
```
> Configure Client:
``` console
nano /etc/hostname 
```
``` Makefile
bacula-client               # for example
```
``` console
nano /etc/hosts
```
```Makefile
10.1.12.250 bacula-client.openstacklocal bacula-client             # if they donâ€™t already exists
```
``` console
nano /etc/bacula/bacula-fd.conf
```
> Edit this Name part to backup-server-hostname and remember the password (ca line 13):
``` Makefile
Director {
    Name = backup-sam-dir
    Password = "HWddrD5uk72XVhZhN7vJgLFvqmw4SnIFb"
    }

ca line 31:
FileDaemon {                          # this is me
    Name = logger-kibana-fd
    FDport = 9102                  # where we listen for the director
    WorkingDirectory = /var/lib/bacula
    Pid Directory = /var/run/bacula
    Maximum Concurrent Jobs = 20
    FDAddress = logger-kibana.openstacklocal
}

ca line 41:
Messages {
    Name = Standard
    director = backup-sam-dir = all, !skipped, !restored
}
```
### Check for errors:
``` console
sudo bacula-fd -tc /etc/bacula/bacula-fd.conf
```
> If echo $? == 0
``` console
sudo service bacula-fd restart
```
> Let's set up a directory that the Bacula Server can restore files to. Create the file structure and lock down the permissions and ownership for security with the following commands:
``` console
sudo mkdir -p /bacula/restore
sudo chown -R bacula:bacula /bacula
sudo chmod -R 700 /bacula
```
### On Bacula-Server:
``` console
sudo nano /etc/bacula/conf.d/filesets.conf
```
- The FileSet Name must be unique
- Include any files or partitions that you want to have backups of
- Exclude any files that you don't want to back up, but were selected as a result of existing within an included file
``` Makefile
FileSet {
    Name = "Home and Etc"
    Include {
            Options {
                signature = MD5
                compression = GZIP
            }
            File = /home
            File = /etc
            File = /root
    }
    Exclude {
        File = /home/bacula/
    }
}
```
> Add Client and Backup Job to Bacula Server:
``` console
sudo nano /etc/bacula/conf.d/clients.conf
```
> Add Client Resource:
``` Makefile
Client {
    Name = logger-kibana-fd
    Address = logger-kibana.openstacklocal
    FDPort = 9102
    Catalog = MyCatalog
    Password = "HWddrD5uk72XVhZhN7vJgLFvqmw4SnIFb"          # password for Remote FileDaemon
    File Retention = 30 days            # 30 days
    Job Retention = 6 months            # six months
    AutoPrune = yes                     # Prune expired Jobs/Files
}
```
> Create a backup job:
``` Makefile
Job {
    Name = "Backuplogger-kibana"
    JobDefs = "DefaultJob"
    Client = logger-kibana-fd
    Pool = RemoteFile
    FileSet="Home and Etc"
}
```
> Verify Director Configuration:
``` console
sudo bacula-dir -tc /etc/bacula/bacula-dir.conf
```
> Restart Bacula Director
``` console
sudo service bacula-director restart
```
> Test Backup Job:
``` console
$ sudo bconsole

run -> 4 -> yes
```
> Perform Restore:
``` console
$ sudo bconsole

restore all -> 5 -> 2 (or depending on the which client to restore to) -> done -> yes
```
