+++
date = "2021-05-10"
title = "Configure Hugo sites with Cookiebot"
slug = "post-4"
authors = ["Samiul Saki"]
categories = ["Tutorial","Post"]
tags = ["HTML"]
+++

If you have a Hugo site you with Gitlab build, you might still need to setup your site to be GDPR compliant, even though it is an static server. Specially if you would like to use Google analytics or GoatCounter to get some statistics of your site. 

You can start by looking into the guide Hugo provides for disabling any sort of cookies on the site to meet the new GDPR regulations.
> General Data Protection Regulation (GDPR) is a regulation in EU law on data protection and privacy for all individuals within the European Union and the European Economic Area. It became enforceable on 25 May 2018.

Hugo itself is a static site generator which makes it already easier to defend your ground. Instead of a server with database driven web site (where you might store data) a static site with HTML files on the disk is much easier to reason with. That being said you still need to make sure that the visitors on your site are not being tracked to any other third-party analytics/monitoring system without having to be aware of it and without giving their full consent before collecting them. Even though those are for some innocent counting of the clicks on the pages for statistisk VIEW ONLY, even they are NOT SOLD or forwarded to any other marketing/sales collecting bot. Because a simple static websites can integrate with external services and you do not want make visitors expose to unneccessary services without they are being aware of it.

Hugo recommend you to imply a simple privacy settings with the default values (preferably all settings disabled) in the site config file i.e., `config.toml` in your root folder. Here is the code which can be added to anywhere in the file (preferably at the beginning) to enable disable privacy settings on the site.

``` yaml
[privacy]
  [privacy.disqus]
    disable = false
  [privacy.googleAnalytics]
    anonymizeIP = false
    disable = false
    respectDoNotTrack = false
    useSessionStorage = false
  [privacy.instagram]
    disable = false
    simple = false
  [privacy.twitter]
    disable = false
    enableDNT = false
    simple = false
  [privacy.vimeo]
    disable = false
    enableDNT = false
    simple = false
  [privacy.youtube]
    disable = false
    privacyEnhanced = false
```

Or use this code which disables every services that is relevent to Hugo:

``` yaml
[privacy]
  [privacy.disqus]
    disable = true
  [privacy.googleAnalytics]
    disable = true
  [privacy.instagram]
    disable = true
  [privacy.twitter]
    disable = true
  [privacy.vimeo]
    disable = true
  [privacy.youtube]
    disable = true
```

> Note: If you are using an YAML or JSON format you can get a copy of the settings in [here](https://gohugo.io/about/hugo-and-gdpr/)

Based on what settings you would like to keep, you need to change the boolean values of *disable =* variables to *true/false*. But this will disable every services even those you like to use, for example: googleAnalytics. It might also break that awesome looking JS script you integrated on your site. We dont want that.

So how do we keep using the services and at the same time make our site GDPR compliant? The answer is Cookies.

I tried to write the simplest method to add the cookies on a Hugo site. I have seen various method that can be implied but this method worked great with my setup. All you need to as prerequistie is to create an account in one of the cookie bot provider. I used [CookieBot](https://www.cookiebot.com/en/) (although there are severals of them) which I find very simple to setup. They do have a [3-step guide](https://www.cookiebot.com/en/help/) which you can follow but used my own method. You are free to follow the guide in the link if that works for you. 

#### Step 1
Create an account in CookieBot website. Don't worry. CookieBot is free to use for one domain per user, and I hope you only have one Hugo site to use. Verify your email and reset the password once requested.
#### Step 2
Log into you account and you will be redirected to `Settings` menu. If not click it from tab on the top.
#### Step 3
Click on the `+` by the `Settings > Domains` field and add the domain name of your site (usually its the url). I chose this website domain name `samiulsaki.info`. Don't worry about https:// option, it will automatically save to the domain name only. Set `Scan frequency` to `Monthly`. Unfortunately free access only have one option, you might have several if you are a paid user.
#### Step 4
Next in the `Dialog` box choose preffered settings for dialog shown for your website visitor. This is the window visitors will see first time they go to your site. You can choose any template you like for this window, you can click on the preview on the left to show you a demo. I suggest the following format for my template.

``` text
Template                : Overlay
Method                  : Explicit Consent
Type                    : Multilevel
Buttons                 : "Reject All / Selection / Allow All"
Checkboxes default mode : [X] Preferences    [X] Statistics    [X] Marketing
```
> Note: The reason I used overlay becuase I wanted to make sure that the visitors definely going to allow/deny before even start browsing my website. Other settings are just to make sure all the options avaialble to the visitors to decide which to keep and what not to share.
#### Step 5
Now we jump to the `Content` sub tab and have a look on the different dialogs based on the languages. These are text the visitors will first encounter when they click on the details option on the consent dialog window. I quite like the default copies of the texts provided but you are free to edit the fields as you require it.
#### Step 6
Now the last and most important setting of the setup. The `Your scripts` tab. As you can see a API key is already generated for your domain group. You can renew it and can also keep a copy of it if you like. For the `Cookie-blocking mode` I will suggest you keep the settings to `Auto`.

Now copy the first script code in the box (HEAD-tag) and head over to your `themes > layouts > partials` folder of the site (local ones of course). Open the `header.html` file and add the code anywhere in your html:
``` html
<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="<YOUR_API_KEY_GOES_HERE>" data-blockingmode="auto" type="text/javascript"></script>
```
> Note: I put it under `{{ end }} and </section>` settings just to confirm that the above script loads in every page of the site no mater which page is loaded first

#### Step 7
Create a HTML file under the content folder of the site, I named mine `CookieDeclaration.html`. Now copy the second scipt from the CookieBot page and add it to this html file you just created. Here is the snippet of my CookieDeclaration.html file with the script.
``` html
<section class="container centered">  
  <header>
    <body style="background-color:rgb(239, 221, 172);"></body>
    <h1>This website uses cookies to make it better. By continuing to use the site, you agree to our use of cookies. </h1>
  </header>
  
  <div id="consent" class="consent">
      <p><script id="CookieDeclaration" src="https://consent.cookiebot.com/<YOUR_API_KEY_GOES_HERE>/cd.js" type="text/javascript" async></script></p>
      <span><button type="button" onclick="window.open('', '_self', ''); window.close();" title="Agree to cookies and close this window">Agree & Close</button></span>
  </div>
</section>
```
> Note: I wanted to make the consent declaration to be available on the every page and would like to open it on a external tab. Also added a button which will close the tab once visitor scroll through the cookie decalarion page. The script loads the page from the CookieBot site and will automatically show the declarion once its exposed to the https. Don't worry if you get the error saying something like this:
``` text
Error: The domain LOCALHOST is not authorized to show the cookie declaration for domain group ID <YOUR_API_KEY_GOES_HERE>. Please add it to the domain group in the Cookiebot Manager to authorize the domain.
```
> Note: This is due to the declarion script auto-generates the texts only for the domain name you created on teh CookieBot. You can ignore this for now.

Now open your `footer.html` file under partials folder (just to make sure this script will be always available in all the pages). Here is a snippet of mine in the footer.html file, your might look different based on your Hugo theme:
``` html
<a style="font-weight:bold;color:darkgrey;font-size:10px;"  href="/downloadables/CookieDeclaration.html" target="popup" onclick="window.open('/downloadables/CookieDeclaration.html','popup','width=1000,height=800, top=100'); return false;">[THATS THE WAY THE COOKIE CRUMBLES]</a>  
```
> Note: My declaration html file is under `content > downloadables` folder

Once done you can of course test it out with this command:
``` console
$ hugo server -D
```
And by opening [http://localhost:1313/](http://localhost:1313/) url in your favourite web-browser.

#### Step 8
Now do a git commit and git push of your site (you know the drill). Open your site and voilà!!

## Happy Cookies!!!


_Update: Unfortunately, free plan for the Cookiebot usage allow only 100 pages and less. After that your free account will be converted to premium plan. You can avoid it by keeping the number of site pages to minimum or getting a Cookiebot premium subscription._