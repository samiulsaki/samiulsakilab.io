+++
date = "2017-05-19"
title = "An Automated Test Framework for Web-Pages using Git, Jenkins, Docker and OpenStack"
slug = "post-7"
authors = ["Samiul Saki"]
categories = ["Project"]
tags = ["Cloud", "DevOps"]
+++

### Project
> ![FWNX1a.md.png](/links/technical_design.png)
>
> In this report, an automated continuous integration suite is created in order to achieve a test framework for web-pages using Git, Jenkins, Docker and OpenStack. The main focus of this framework is the automation of the entire process. Using this framework the web-developers can observe if the newly modified HTML codes (which are pushed from Git-repository) are working successfully or not before it is released to the production. 
>
> Upon successful test build, the web- server is automatically running in to production which ensures the complete automated continuous integration test suite for web-pages.
>
> [Link to PDF](/links/project_05_2017.pdf)
