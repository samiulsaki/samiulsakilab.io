+++
title = "About Me"
slug = "about" 
+++

> _Hi,_
> 
> _Thank you for visiting my page. I am currently working as Automation Engineer, within DevOps and Cloud technology. I have achieved two masters within System Administration and Communication Technology. I have several courses and certifications on AWS, Terraform and Kubernetes. In addition, I have more than seven years of experience in Norway and abroad, in both large and small companies and has splendid knowledge of automation, integration, development and DevOps._
>
> _I am positive, hardworking and effective. My ability to learning fast and multitask are some of the reasons why I would be an excellent choice for any company. I am a fast learner, patient, co-operative with my co-workers and quick at problem solving. My job objective is to achieve an appropriate job in a reputed organisation that offers highly structured working environment & provides opportunities to develop a firm career in the organisation. To be a part of a dynamic organization as a valuable employee to help organization to reach the peak performance as well as to accomplish its goal more efficiently with the help of my knowledge gained through work experiences._
>
> _I am punctual, responsible and flexible with my working hours. I firmly believe that, with my experience and work rate, I can bring a valuable resource to any business and am excited about this prospect and potential._
>
> _I become acquainted with issues in short time, gather new knowledge, get familiar with technology and translates them into feasible and stable results. I am eager to work on assignments where I can put theories into practice. I am outgoing and social and belong to the new generation of DevOps, who is very efficient to develop and run operational services in automated manner whenever its possible. Some of my strengths as a DevOps engineer are coordinating and organizing the workload. In the feedbacks I have received from my employers and clients, I have been perceived as responsible and disciplined, motivated, proactive, solution- and goal-oriented, stands up for my workethics and is very capable of deliver solutions on time. I am efficient, systematic and accurate in the workplace. I am patient and cooperative with my colleagues and have wonderful communication skills._

* * *
* * *
## Proficiency
###### DevOps Tools: 
> OpenStack, Puppet, Foreman, Logstash, Fluentd, Jenkins, Sensu, ElasticSearch, Backup Solution: (Bacula, Amanda, Rsync), Docker, Terraform, Ansible, Consul, Apache, Nginx, OpenSSL, Agile Task Grooming, Scrum, Github actions, Gitlab CI/CD
###### Programming Skills:
> Python, Bash, PHP, Embedded Systems: FPGA + Xilinx, Contiki, C++, MySQL, Golang, Modulo, AutoCAD, JavaScript, Visual Basic
###### Platform Solutions:
> AWS, Openstack, GCP, Azure AD, RHEL, NextCloud, OwnCloud, VMware
###### Administration Tools:
> Munki, Zimbra Mail Server, Proxmox, CEPH, MacOS Server, Navision, ZFS Cluster, G Suite, Office365, UniFi Switches, Microsoft Active Directory
###### Monitoring Tools:
> Observium, Check_MK, Zabbix, StatusCake, Splunk, AppDynamics, Datadog, Application Manager, Kibana, Grafana, Prometheus
###### Networking Tools:
> Wireless Networking, Routing, LAN, WLAN, VLAN, VTP, STP, Switches: (Cisco, Dell, UniFi), Routing Protocols, Wireless Sensor Networks, DNS, DHCP, IPTable Rules, Fireproof
###### Service Oriented Architecture / API Skills:
> REST, SOAP, XML Schema, WSDL
###### Apple Development Tools:
> Apple Configurator, AutoPkg, OSAScript, Profile-Creator, PKGUtil, Pacifist
###### Other IT Skills:
> Linux, Mac OS, Windows, Hardware and System Troubleshooting, Raspberry Pi, Arduino IDE, Gammu SMS Server, Wireless Security, Release Engineering, Version Control (Git), Internet Security, Quagga, GNS3, Syslog, NS2, Packet Tracer, Wireshark

* * *
* * *

## Membership/Volunteer
* Tekna
* Røde Kors
* Natteravene (Night Petrol)

* * *
* * *

## Causes I Care About
> Animal Welfare, Civil Rights and Social Action, Environment, Human Rights, Disaster and Humanitarian Relief

* * *
* * *

## Languages
Lang-Code    | Proficiency
------------- | -------------
*en-GB*       | *Professional and Fluent*
*nb-NO*       | *Professional and Fluent*
